import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner myScan = new Scanner(System.in);

        System.out.println("Введите температуру в °C:");
        int temperature = myScan.nextInt();

        System.out.println("Введите скорость ветра в м/c:");
        int windSpeed = myScan.nextInt();

        System.out.println("Сейчас идёт дождь? [y/n]");
        char isRaining = (char)System.in.read();

        if (isRaining == 'y') {
            System.out.println("Совет: Сейчас не лучшее время для прогулки.");
        } else if (windSpeed >= 15) {
            System.out.println("Совет: Большая скорость ветра. Лучше не выходить на улицу");
        } else if (temperature < 10){
            System.out.println("Совет: На улице слишком холодно! Лучше остаться дома или одеться потеплее.");
        } else if (temperature > 25) {
            System.out.println("Совет: На улице очень жарко! Оставайтесь дома или возьмите с собой что-то освежающее.");
        } else {
            System.out.println("Совет: Идеальное время для прогулки!");
        }
    }
}
